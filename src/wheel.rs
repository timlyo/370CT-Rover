//! Contains all wheel logic

use std::fmt;
use rand::{thread_rng, Rng};

use logger::{LOGGER, Tag};

const SUCCESS_CHANCE: i32 = 90; // On  per wheel basis so actual chance is much lower

#[derive(Clone, PartialEq, Debug)]
pub enum State {
    Running,
    Blocked,
    FreeWheeling,
    Sinking,
}

#[derive(PartialEq)]
pub struct Wheel {
    pub state: State,
}

impl Wheel {
    /// Creates a new wheel in the running state
    pub fn new() -> Wheel {
        Wheel { state: State::Running }
    }

    /// Attempts to correct a sinking of freewheeling wheel by calling the raise or lower method
    /// Returns `true` if the operation succeded, `false` otherwise
    pub fn correct_wheel_pos(&mut self) -> bool {
        match self.state {
            State::Running => true,
            State::Blocked => true,
            State::Sinking => self.raise(),
            State::FreeWheeling => self.lower(),
        }
    }

    /// Attemps to raise the wheel and fix a sinking problem
    pub fn raise(&mut self) -> bool {
        if self.state != State::Sinking {
            panic!();
        }

        if thread_rng().gen_range(0, 100) < SUCCESS_CHANCE {
            self.state = State::Running;
            LOGGER.add_record(Tag::Wheel, "Raising wheels ✓");
            return true;
        }

        LOGGER.add_record(Tag::Wheel, "Raising wheels ✕");
        return false;
    }

    /// Attemps to lower the wheel and fix a freewheeling problem
    pub fn lower(&mut self) -> bool {
        if self.state != State::FreeWheeling {
            panic!();
        }

        if thread_rng().gen_range(0, 100) < SUCCESS_CHANCE {
            self.state = State::Running;
            LOGGER.add_record(Tag::Wheel, "Lower wheels ✓");
            return true;
        }

        LOGGER.add_record(Tag::Wheel, "Lower wheels ✕");
        return false;
    }

    /// Returns `true` if the wheel is not blocked
    pub fn can_rotate(&self) -> bool {
        match self.state {
            State::Blocked => false,
            _ => true,
        }
    }
}

impl fmt::Debug for Wheel {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let state_string = match self.state {
            State::Running => "✔",
            State::Blocked => "✖",
            State::FreeWheeling => "⟳",
            State::Sinking => "地",
        };
        write!(f, "Wheel {}:{:?}", state_string, self.state)
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use rover::Rover;
    use logger::Logger;
    use std::sync::Arc;

    #[test]
    fn fix_sinking() {
        let mut rover = Rover::new();
        rover.set_wheel_state(State::Sinking, 63);

        for wheel in rover.wheels {
            let mut wheel = wheel.lock().unwrap();
            assert_eq!(wheel.state, State::Sinking);

            // loop until returns true
            while !wheel.correct_wheel_pos() {}
            assert_eq!(wheel.state, State::Running);
        }
    }

    #[test]
    fn fix_hole() {
        let mut rover = Rover::new();
        rover.set_wheel_state(State::FreeWheeling, 63);

        for wheel in rover.wheels {
            let mut wheel = wheel.lock().unwrap();
            assert_eq!(wheel.state, State::FreeWheeling);

            // loop until returns true
            while !wheel.correct_wheel_pos() {}
            assert_eq!(wheel.state, State::Running);
        }
    }
}
