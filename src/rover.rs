//! Rover module

use std::fmt;

use std::sync::atomic::{AtomicI64, AtomicBool, Ordering};
use std::sync::{Arc, Mutex, Condvar};
use std::thread;
use std::time::Duration;
use std::sync::mpsc::{Sender, Receiver};

use hyper::Client;

use wheel;
use wheel::Wheel;
use logger::{LOGGER, Tag};

/// Struct to hold all rover information
pub struct Rover {
    /// Store of all the rover's wheels
    pub wheels: Vec<Arc<Mutex<Wheel>>>,
    /// Tracks completeness of the rover,
    /// used as a flag by other threads for when to stop processing
    pub done: AtomicBool,
    /// used to track rover movement and work out when the task is complete
    pub distance: AtomicI64,
}

/// Runs the loop for sinking fixer
/// Detailed information is available in docs
pub fn sinking_fixer(rover: Arc<Mutex<Rover>>,
                     wheel_sinking: Receiver<Vec<usize>>,
                     handled: Sender<bool>) {
    loop {
        let wheels: Vec<usize> = match wheel_sinking.recv() {
            Ok(wheels) => wheels,
            Err(_) => break,
        };
        let mut success = true;
        for wheel in wheels {
            if let Ok(rover) = rover.lock() {
                if !rover.wheels[wheel].lock().unwrap().raise() {
                    // if any wheel failed to be corrected then it was unsuccessful
                    success = false;
                }
            }
        }
        let _ = handled.send(success);
    }
    LOGGER.add_record(Tag::Sys, "sinking_fixer done");
}

/// Runs the loop for freewheeling fixer
/// Detailed information is available in docs
pub fn freewheeling_fixer(rover: Arc<Mutex<Rover>>,
                          wheel_freewheeling: Receiver<Vec<usize>>,
                          handled: Sender<bool>) {
    loop {
        let wheels: Vec<usize> = match wheel_freewheeling.recv() {
            Ok(wheels) => wheels,
            Err(_) => break,
        };
        let mut success = true;
        for wheel in wheels {
            if let Ok(rover) = rover.lock() {
                if !rover.wheels[wheel].lock().unwrap().lower() {
                    // if any wheel failed to be corrected then it was unsuccessful
                    success = false;
                }
            }
        }
        let _ = handled.send(success);
    }
    LOGGER.add_record(Tag::Sys, "freewheeling_fixer done");
}

/// Runs the loop for rover movement
/// Detailed information is available in docs
pub fn mover(rover: Arc<Mutex<Rover>>, moved_cond: Arc<(Condvar, Mutex<()>)>) {
    loop {
        if let Ok(mut rover) = rover.lock() {
            rover.progress();
            moved_cond.0.notify_all();
            if rover.is_done() {
                break;
            }
        }
        thread::sleep(Duration::from_millis(200));
    }
    LOGGER.add_record(Tag::Sys, "movement_monitor done");
}

/// Runs the loop for wheel monitoring
/// Detailed information is available in docs
pub fn wheel_monitor(rover: Arc<Mutex<Rover>>,
                     help_needed: Sender<()>,
                     help_completed: Receiver<()>,
                     wheel_sinking: Sender<Vec<usize>>,
                     wheel_not_sinking: Receiver<bool>,
                     wheel_freewheeling: Sender<Vec<usize>>,
                     wheel_not_freewheeling: Receiver<bool>) {
    loop {
        let mut sinking = Vec::new();
        let mut freewheeling = Vec::new();

        if let Ok(rover) = rover.lock() {
            if rover.is_done() {
                break;
            }

            for (index, wheel) in rover.wheels.iter().enumerate() {
                if let Ok(wheel) = wheel.lock() {
                    match wheel.state {
                        wheel::State::Sinking => sinking.push(index),
                        wheel::State::FreeWheeling => freewheeling.push(index),
                        _ => {}
                    }
                }
            }
        }

        let _ = wheel_sinking.send(sinking);
        let _ = wheel_freewheeling.send(freewheeling);

        let sinking_fixed = wheel_not_sinking.recv().unwrap_or(false);
        let freewheeling_fixed = wheel_not_freewheeling.recv().unwrap_or(false);

        let handled = sinking_fixed && freewheeling_fixed;
        if !handled {
            let _ = help_needed.send(());
            let _ = help_completed.recv();
        }
        thread::sleep(Duration::from_millis(100));
    }
    LOGGER.add_record(Tag::Sys, "wheel_monitor done");
}

impl Rover {
    /// Creates a new Rover instance with 6 wheels, done initialised to false, and 0 distance
    pub fn new() -> Rover {
        let wheels = (0..6).map(|_| Arc::new(Mutex::new(Wheel::new()))).collect();

        Rover {
            wheels: wheels,
            done: AtomicBool::new(false),
            distance: AtomicI64::new(0),
        }
    }

    pub fn new_wrapped() -> Arc<Mutex<Rover>> {
        Arc::new(Mutex::new(Rover::new()))
    }

    /// Uses `affected_wheels` as a bitmask to set the state of multiple wheels to the passed in
    /// state
    /// # Panics
    /// Panics if `affected_wheels` represents a bitmask greater than the available wheels i.e.
    /// `log(2, affected_wheels) > wheels.len()`
    ///
    pub fn set_wheel_state(&mut self, state: wheel::State, affected_wheels: u8) {
        if affected_wheels >= 2u8.pow(self.wheels.len() as u32) {
            panic!("Rover doesn't have that many wheels");
        }
        self.wheels = self.wheels
            .iter()
            .enumerate()
            .map(|(index, wheel)| if (affected_wheels & (1 << index)) != 0 {
                     Arc::new(Mutex::new(Wheel { state: state.clone() }))
                 } else {
                     wheel.clone()
                 })
            .collect()
    }

    /// Wrapper around rover.done handles the atomic variable
    #[inline]
    pub fn is_done(&self) -> bool {
        self.done.load(Ordering::Relaxed)
    }

    /// Scans all wheels and corrects positioning returns false on failure
    pub fn monitor_wheels(&mut self) -> bool {
        LOGGER.add_record(Tag::Rover, "Checking wheels");
        // Assume correct until proven otherwise
        let mut all_success = true;
        // Iterate through wheels and attempt to prove otherwise
        for wheel in &self.wheels {
            if !wheel.lock().unwrap().correct_wheel_pos() {
                all_success = false;
            }
        }

        if all_success {
            LOGGER.add_record(Tag::Rover, "Wheels Corrected");
            return true;
        } else {
            LOGGER.add_record(Tag::Rover, "Not all wheels Corrected");
            return false;
        }
    }

    /// Checks if all wheels are able to rotate
    /// Returns false if a single wheel cannot i.e. `wheel.state == Blocked`
    pub fn can_move(&self) -> bool {
        for wheel in &self.wheels {
            if let Ok(wheel) = wheel.lock() {
                if !wheel.can_rotate() {
                    return false;
                }
            }
        }
        true
    }

    /// Set all wheel's states to unblocked
    pub fn set_wheels_unblocked(&mut self) {
        let wheels = self.wheels
            .iter()
            .map(|wheel| if let Ok(wheel_lock) = wheel.lock() {
                     if wheel_lock.state == wheel::State::Blocked {
                         // create new wheel, state wil be reset this way
                         Arc::new(Mutex::new(Wheel::new()))
                     } else {
                         // Not blocked just return current wheel
                         wheel.clone()
                     }
                 } else {
                     // Mutex poisoned
                     panic!();
                 })
            .collect();

        self.wheels = wheels;
    }

    /// Attempt to move rover forward, does nothing if wheels are blocked
    /// Sets `rover.done` to `true` when `rover.distance == 200`
    pub fn progress(&mut self) {
        if self.can_move() {
            self.distance.fetch_add(20, Ordering::Relaxed);
        } else {
            LOGGER.add_record(Tag::Rover, "Wheels blocked, reversing");
            self.set_wheels_unblocked();
            self.distance.fetch_sub(20, Ordering::Relaxed);
        }

        let log = format!("Rover at {}cm", self.distance.load(Ordering::Relaxed));
        LOGGER.add_record(Tag::Rover, &log);

        if self.distance.load(Ordering::Relaxed) >= 200 {
            LOGGER.add_record(Tag::Rover, "Rover has travelled 200cm");
            self.done.store(true, Ordering::Relaxed);
        }
    }

    /// Prints some useless rubbish about asking for help, waits and then fixes the wheels anyway
    pub fn request_help(&mut self) {
        LOGGER.help();
        LOGGER.add_record(Tag::Rover, "Requesting help from base");

        // Make request
        let client = Client::new();
        let res = !match client.get("http://178.79.161.168/rover/?tim").send(){
            Ok(res) => Some(res).is_none(),
            Err(e) => false
        };
        
        #[cfg(not(test))]
        thread::sleep(Duration::from_secs(1));
        LOGGER.add_record(Tag::Rover, "Request sent");
        #[cfg(not(test))]
        thread::sleep(Duration::from_secs(1));
        LOGGER.add_record(Tag::Rover, "Response recieved");
        #[cfg(not(test))]
        thread::sleep(Duration::from_secs(1));

        // correct all wheels
        if res {
            for wheel in &self.wheels {
                if let Ok(mut wheel) = wheel.lock() {
                    wheel.state = wheel::State::Running;
                }
            }
            LOGGER.add_record(Tag::Rover, "Help solved problem");
        }else {
            LOGGER.add_record(Tag::Rover, "NASA have forsaken us");
        }
    }
}

impl fmt::Debug for Rover {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let wheels: String = self.wheels
            .iter()
            .enumerate()
            .map(|(index, wheel)| format!("\t{}:{:?}\n", index + 1, wheel))
            .collect();
        write!(f, "Distance: {:?}\n{}", self.distance, wheels)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use events::{Event, process_event};

    #[test]
    fn set_wheels_modified() {
        let mut rover = Rover::new();

        rover.set_wheel_state(wheel::State::Blocked, 3);

        println!("{:?}", rover);
        assert_eq!(rover.wheels[0].lock().unwrap().state, wheel::State::Blocked);
        assert_eq!(rover.wheels[1].lock().unwrap().state, wheel::State::Blocked);
    }

    #[test]
    fn set_all_wheels() {
        let mut rover = Rover::new();

        rover.set_wheel_state(wheel::State::Sinking, 63);

        println!("{:?}", rover);
        for wheel in rover.wheels {
            let wheel = wheel.lock().unwrap();
            assert_eq!(wheel.state, wheel::State::Sinking);
        }
    }

    #[cfg(debug_assertions)]
    //only run in debug mode
    #[test]
    #[should_panic]
    fn check_wheel_set() {
        let mut rover = Rover::new();

        // There aren't 10 wheels therefore this should panic
        let num_wheels = 10;
        rover.set_wheel_state(wheel::State::Running, 2u8.pow(num_wheels));
    }

    #[test]
    fn request_help() {
        let mut rover = Rover::new();

        rover.set_wheel_state(wheel::State::Sinking, 63);

        println!("{:?}", rover);
        rover.request_help();
        for wheel in rover.wheels {
            if let Ok(wheel) = wheel.lock() {
                assert_eq!(wheel.state, wheel::State::Running);
            }
        }
    }

    #[test]
    fn rover_completes() {
        let mut rover = Rover::new();

        for _ in 0..10 {
            rover.progress();
        }

        assert_eq!(rover.distance.load(Ordering::Relaxed), 200);
        assert!(rover.is_done());
    }

    #[test]
    fn rover_reverse() {
        let mut rover = Rover::new();

        rover.progress();
        assert_eq!(rover.distance.load(Ordering::Relaxed), 20);

        let rover = Arc::new(Mutex::new(rover));
        process_event(Event::Rock(63), rover.clone());
        let mut rover = rover.lock().unwrap(); // Can't fail, only thread with access
        rover.progress();

        assert_eq!(rover.distance.load(Ordering::Relaxed), 0);
    }
}
