//! Containing module for 370CT assignment
#![feature(integer_atomics)]

extern crate rand;
extern crate time;
#[macro_use]
extern crate lazy_static;
extern crate colored;
extern crate hyper;

pub mod wheel;
pub mod rover;
pub mod events;
pub mod logger;

use rover::Rover;
use logger::{LOGGER, Tag};

use events::*;

use std::sync::{Arc, Mutex, Condvar};
use std::sync::mpsc::{channel, Sender, Receiver};
use std::thread;
use std::thread::JoinHandle;
use std::time::Duration;


fn main() {
    // Create data
    let event_list = Arc::new(Mutex::new(Vec::new()));
    let rover = Rover::new_wrapped();

    // Create synchronisation structures
    let event_condition = Arc::new(Condvar::new());
    let moved_condition = Arc::new((Condvar::new(), Mutex::new(())));
    let help_needed = channel();
    let help_completed = channel();
    let wheel_sinking = channel();
    let wheel_not_sinking = channel();
    let wheel_freewheeling = channel();
    let wheel_not_freewheeling = channel();


    // Start threads
    let threads = vec![event_producer(rover.clone(), event_list.clone(), event_condition.clone()),
                       event_consumer(rover.clone(),
                                      event_list,
                                      event_condition,
                                      moved_condition.clone()),

                       setup_wheel_monitor(rover.clone(),
                                           help_needed.0,
                                           help_completed.1,
                                           wheel_sinking.0,
                                           wheel_not_sinking.1,
                                           wheel_freewheeling.0,
                                           wheel_not_freewheeling.1),
                       setup_rover_mover(rover.clone(), moved_condition),
                       setup_help_requester(rover.clone(), help_needed.1, help_completed.0),
                       setup_sinking_fixer(rover.clone(), wheel_sinking.1, wheel_not_sinking.0),
                       setup_freewheeling_fixer(rover.clone(),
                                                wheel_freewheeling.1,
                                                wheel_not_freewheeling.0)];

    for thread in threads {
        let _ = thread.join();
    }

    LOGGER.print_stats();
}

/// Creates a thread to run the `sinking_fixer` function
fn setup_sinking_fixer(rover: Arc<Mutex<Rover>>,
                       wheel_sinking: Receiver<Vec<usize>>,
                       handled: Sender<bool>)
                       -> JoinHandle<()> {
    let name = "sinking_fixer".to_string();
    thread::Builder::new()
        .name(name)
        .spawn(move || rover::sinking_fixer(rover.clone(), wheel_sinking, handled))
        .unwrap()
}

/// Creates a thread to run the `freewheeling_fixer` function
fn setup_freewheeling_fixer(rover: Arc<Mutex<Rover>>,
                            wheel_freewheeling: Receiver<Vec<usize>>,
                            handled: Sender<bool>)
                            -> JoinHandle<()> {
    thread::Builder::new()
        .name("freewheeling_fixer".to_string())
        .spawn(move || rover::freewheeling_fixer(rover.clone(), wheel_freewheeling, handled))
        .unwrap()
}

/// Creates a thread to run the `rover_mover` function
fn setup_rover_mover(rover: Arc<Mutex<Rover>>,
                     moved_cond: Arc<(Condvar, Mutex<()>)>)
                     -> JoinHandle<()> {
    thread::Builder::new()
        .name("rover_mover".to_string())
        .spawn(move || rover::mover(rover, moved_cond))
        .unwrap()
}

/// Creates a thread to run the `wheel_monitor` function
fn setup_wheel_monitor(rover: Arc<Mutex<Rover>>,
                       help_needed: Sender<()>,
                       help_completed: Receiver<()>,
                       wheel_sinking: Sender<Vec<usize>>,
                       wheel_not_sinking: Receiver<bool>,
                       wheel_freewheeling: Sender<Vec<usize>>,
                       wheel_not_freewheeling: Receiver<bool>)
                       -> JoinHandle<()> {
    thread::Builder::new()
        .name("wheel_monitor".to_string())
        .spawn(move || {
            rover::wheel_monitor(rover,
                                 help_needed,
                                 help_completed,
                                 wheel_sinking,
                                 wheel_not_sinking,
                                 wheel_freewheeling,
                                 wheel_not_freewheeling)
        })
        .unwrap()
}

/// Creates a thread to run the help requesting logic
fn setup_help_requester(rover: Arc<Mutex<Rover>>,
                        help_needed: Receiver<()>,
                        help_completed: Sender<()>)
                        -> JoinHandle<()> {
    thread::Builder::new()
        .name("help_requester".to_string())
        .spawn(move || {
            loop {
                // wait for help request
                match help_needed.recv() {
                    Ok(_) => {}
                    // end thread if channel is closed
                    Err(_) => break,

                }

                if let Ok(mut rover) = rover.lock() {
                    rover.request_help();
                }

                let _ = help_completed.send(());
            }
            LOGGER.add_record(Tag::Sys, "help_requester done");
        })
        .unwrap()
}
