//! Handles event creation and consumption

use rand::{Rng, thread_rng};
use std::thread;

use rover::Rover;
use wheel;
use logger::{LOGGER, Tag};

use std::sync::{Arc, Mutex, Condvar};
use std::time::Duration;

/// An instance of an event with a bitmask for which wheels it affects
#[derive(Debug)]
pub enum Event {
    Rock(u8),
    Sand(u8),
    Hole(u8),
}

/// Wrapper for rover.done, handles mutex locking
fn rover_done(rover: &Arc<Mutex<Rover>>) -> bool {
    match rover.lock() {
        Ok(rover) => rover.is_done(),
        Err(_) => panic!("Poisoned mutex"),
    }
}

/// Takes events from the list and modifies the rover instance accordingly
pub fn event_consumer(rover: Arc<Mutex<Rover>>,
                      event_list: Arc<Mutex<Vec<Event>>>,
                      event_cond: Arc<Condvar>,
                      rover_moved_cond: Arc<(Condvar, Mutex<()>)>)
                      -> thread::JoinHandle<()> {
    thread::spawn(move || {
        loop {
            if rover_done(&rover) {
                break;
            }

            if let Ok(mut events) = event_list.lock() {
                match events.pop() {
                    Some(event) => {
                        process_event(event, rover.clone());
                        event_cond.notify_one();
                    }
                    None => {
                        event_cond.wait(events).unwrap();
                    }
                }
            }

            // Wait until rover has moved before next event
            if let Ok(moved) = rover_moved_cond.1.lock() {
                let _ = rover_moved_cond.0.wait(moved).unwrap();
            }

            // Allow rover thread to process event
            thread::sleep(Duration::from_millis(200));
        }
        LOGGER.add_record(Tag::Sys, "event_consumer done");
    })
}

/// Modifies the rover instance in respect to the single event that is passed to it
///
/// Logs the affected wheels with their bitmask formatted with {:06b}
pub fn process_event(event: Event, rover: Arc<Mutex<Rover>>) {
    LOGGER.event();
    if let Ok(mut rover) = rover.lock() {
        match event {
            Event::Rock(i) => {
                rover.set_wheel_state(wheel::State::Blocked, i);
                LOGGER.add_record(Tag::Event, &format!("Wheels blocked {:06b}", i));
            }
            Event::Sand(i) => {
                rover.set_wheel_state(wheel::State::Sinking, i);
                LOGGER.add_record(Tag::Event, &format!("Wheels sinking {:06b}", i));
            }
            Event::Hole(i) => {
                rover.set_wheel_state(wheel::State::FreeWheeling, i);
                LOGGER.add_record(Tag::Event, &format!("Wheels freewheeling {:06b}", i));
            }
        }
    }
}

/// Creates events that are later handled by event_cosumer
///
/// Will wait on condition variables to limit the amount of uneeded events created
pub fn event_producer(rover: Arc<Mutex<Rover>>,
                      event_list: Arc<Mutex<Vec<Event>>>,
                      event_cond: Arc<Condvar>)
                      -> thread::JoinHandle<()> {
    thread::spawn(move || {
        loop {
            if rover_done(&rover) {
                break;
            }

            // Generate event
            if let Ok(mut events) = event_list.lock() {
                if events.len() >= 10 {
                    LOGGER.add_record(Tag::Event, "Event queue is full");
                    events = event_cond.wait(events).unwrap();
                }
                
                let num_wheels = rover.lock().unwrap().wheels.len() as u32;
                let num_bits = 2u8.pow(num_wheels);
                let affected_wheels = thread_rng().gen_range(0, num_bits);

                // randomly generate event type from number
                match thread_rng().gen_range(0, 3) {
                    0 => events.push(Event::Rock(affected_wheels)),
                    1 => events.push(Event::Sand(affected_wheels)),
                    2 => events.push(Event::Hole(affected_wheels)),
                    _ => {}
                }

                event_cond.notify_all();
            }

            thread::sleep(Duration::from_millis(300));
        }
        LOGGER.add_record(Tag::Sys, "event_producer done");
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_process_event_sand(){
        let rover = Rover::new_wrapped();

        let event = Event::Sand(63);
        process_event(event, rover.clone());

        let rover = rover.lock().unwrap();
        for wheel in &rover.wheels{
            if let Ok(wheel) = wheel.lock(){
                assert_eq!(wheel.state, wheel::State::Sinking);
            }else{
                panic!("Wheel lock failed");
            }
        }
    }

    #[test]
    fn test_process_event_hole(){
        let rover = Rover::new_wrapped();

        let event = Event::Hole(63);
        process_event(event, rover.clone());

        let rover = rover.lock().unwrap();
        for wheel in &rover.wheels{
            if let Ok(wheel) = wheel.lock(){
                assert_eq!(wheel.state, wheel::State::FreeWheeling);
            }else{
                panic!("Wheel lock failed");
            }
        }
    }

    #[test]
    fn test_process_event_rock(){
        let rover = Rover::new_wrapped();

        let event = Event::Rock(63);
        process_event(event, rover.clone());

        let rover = rover.lock().unwrap();
        for wheel in &rover.wheels{
            if let Ok(wheel) = wheel.lock(){
                assert_eq!(wheel.state, wheel::State::Blocked);
            }else{
                panic!("Wheel lock failed");
            }
        }
    }
}
