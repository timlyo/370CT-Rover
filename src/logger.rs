//! Handles information capture

use time;
use time::Duration;
use std::sync::RwLock;
use std::fmt;
use colored::*;

/// Used to group common log items
#[derive(Debug)]
pub enum Tag {
    Sys,
    Event,
    Rover,
    Wheel,
}

impl fmt::Display for Tag {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Tag::Sys => write!(f, "Sys"),
            Tag::Event => write!(f, "Event"),
            Tag::Rover => write!(f, "Rover"),
            Tag::Wheel => write!(f, "Wheel"),
        }
    }
}

lazy_static! {
    /// Global static logger instance
    pub static ref LOGGER: Logger = Logger::new();
}

#[derive(Debug)]
pub struct Record {
    /// Time that the record was created. Counted as an offset from startup
    time: Duration,
    /// Contents of the log
    event: String,
    /// Type of the log
    tag: Tag,
}

impl fmt::Display for Record {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let millis = self.time.num_milliseconds();
        let record = format!("[{:<4}s] ({}) {}", millis, self.tag, self.event);

        match self.tag {
            Tag::Sys => write!(f, "{}", record.yellow()),
            Tag::Event => write!(f, "{}", record.blue()),
            Tag::Rover => write!(f, "{}", record.green()),
            Tag::Wheel => write!(f, "{}", record.magenta()),
        }
    }
}

/// Struct to hold program runtime statistics
pub struct Stats {
    event_count: u32,
    help_count: u32,
}

impl Stats {
    /// Create new `Stats` struct with `event_count` and `help_count` at 0
    fn new() -> Stats {
        Stats {
            event_count: 0,
            help_count: 0,
        }
    }
}

/// Container for Records
pub struct Logger {
    start_time: RwLock<time::Tm>,
    records: RwLock<Vec<Record>>,
    stats: RwLock<Stats>,
}

impl Logger {
    /// Create a new logger
    ///
    /// Automatically sets the start time and adds a `Sys` log for the program starting
    pub fn new() -> Logger {
        let logger = Logger {
            start_time: RwLock::new(time::now()),
            records: RwLock::new(Vec::new()),
            stats: RwLock::new(Stats::new()),
        };

        logger.add_record(Tag::Sys, "Start");
        logger
    }

    /// Appends a new record
    ///
    /// Automatically handles storing the time that the record was created
    pub fn add_record(&self, tag: Tag, event: &str) {
        if let Ok(mut records) = self.records.write() {
            let time = match self.start_time.read() {
                Ok(start_time) => time::now() - *start_time,
                Err(_) => panic!(),
            };

            let record = Record {
                time: time,
                event: event.to_string(),
                tag: tag,
            };
            println!("{}", record);
            records.push(record);
        }
    }

    /// Sorts the times that the logs where created
    ///
    /// Normally not needed if all records are added with automatic timing
    pub fn sort_log(&self) {
        if let Ok(mut records) = self.records.write() {
            records.sort_by_key(|r| r.time);
        }
    }

    /// Formats and prints each record contained in the log
    pub fn print_log(&self) {
        if let Ok(records) = self.records.read() {
            for record in records.iter() {
                println!("{}", record);
            }
        }
    }

    /// Print the contents of the stats struct
    pub fn print_stats(&self) {
        if let Ok(stats) = self.stats.read() {
            println!("Encountered {} events", stats.event_count);
            println!("Requested help {} times", stats.help_count);
        }
    }

    /// Increment `event_count` by 1, called whenever an event is encountered
    pub fn event(&self) {
        if let Ok(mut stats) = self.stats.write() {
            stats.event_count += 1;
        }
    }

    /// Increment `help_count` by 1, called whenever help is requested
    pub fn help(&self) {
        if let Ok(mut stats) = self.stats.write() {
            stats.help_count += 1;
        }
    }
}
