# Rover
- 6 wheel objects, one thread(maybe one each) checking each wheel and lifting as required
	- can detect stopped for sand and raises
	- can detect freewheeling for hole and lower
- Proximity sensor for rock infront
	- calls wheels to reverse

# Wheel
- enum with 3 states {Running, Stopped, Freewheeling}


- One thread generating events and pushing them into a central event queue
	- Stops if event queue length > %insert num here%
- One thread reading events and modifying the rover object to react to them
	- Waits if event queue is empty
- Central event queue is a mutex locked vector
